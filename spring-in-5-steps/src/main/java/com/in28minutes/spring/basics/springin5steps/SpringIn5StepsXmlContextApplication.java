package com.in28minutes.spring.basics.springin5steps;

import com.in28minutes.spring.basics.springin5steps.xml.XmlPersonDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class SpringIn5StepsXmlContextApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringIn5StepsXmlContextApplication.class);
    public static void main(String[] args) {

        try(ClassPathXmlApplicationContext applicationContext =
                    new ClassPathXmlApplicationContext("applicationContext.xml")){
        // Application Context contains all the beans

            LOGGER.info("Beans Loaded -> {}", (Object) applicationContext.getBeanDefinitionNames());
            // -> [xmlJdbcConnection, xmlPersonDAO]
            XmlPersonDAO personDAO = applicationContext.getBean(XmlPersonDAO.class);
            LOGGER.info("{}, {}", personDAO, personDAO.getXmlJdbcConnection());
        }
    }

}
