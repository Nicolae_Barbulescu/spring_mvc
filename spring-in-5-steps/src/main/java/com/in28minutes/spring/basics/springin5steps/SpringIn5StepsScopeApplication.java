package com.in28minutes.spring.basics.springin5steps;

import com.in28minutes.spring.basics.springin5steps.scope.PersonDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan
public class SpringIn5StepsScopeApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringIn5StepsScopeApplication.class);

    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext applicationContext =
                     new AnnotationConfigApplicationContext(SpringIn5StepsScopeApplication.class)) {

            PersonDAO personDAO = applicationContext.getBean(PersonDAO.class);

            PersonDAO personDAO1 = applicationContext.getBean(PersonDAO.class);

            LOGGER.info("{}", personDAO);
            LOGGER.info("{}", personDAO.getJdbcConnection());

            LOGGER.info("{}", personDAO1);
            LOGGER.info("{}", personDAO1.getJdbcConnection());
        }
    }
}
